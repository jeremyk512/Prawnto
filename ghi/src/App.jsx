// This makes VSCode check types as if you are using TypeScript
//@ts-check
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import Header from './components/header'
import './App.css'
import Gradebook from './components/gradeBook'
import Manage from './components/manage'
import Explore from './components/explore'
import LoginForm from './components/login'
import { AuthProvider } from '@galvanize-inc/jwtdown-for-react'
import Signup from './components/Signup'
import MakeExam from './components/makeexam'
import AttemptedExam from './components/attemptedexam'
import CreateQuestion from './components/createquestion'
import TakeExam from './components/takeExams'
import Heropage from './components/heroPage'

// All your environment variables in vite are in this object

// When using environment variables, you should do a check to see if
// they are defined or not and throw an appropriate error message
const API_HOST = import.meta.env.VITE_API_HOST

if (!API_HOST) {
    throw new Error('VITE_API_HOST is not defined')
}
function App() {
    return (
        <>
            <AuthProvider baseUrl={API_HOST}>
                <BrowserRouter>
                    <Header />
                    <Routes>
                        <Route
                            path="/gradebook/:exam_id"
                            element={<Gradebook />}
                        />
                        <Route path="/manage" element={<Manage />} />
                        <Route path="/explore" element={<Explore />} />
                        <Route path="/Signup" element={<Signup />} />
                        <Route path="/login" element={<LoginForm />} />
                        <Route path="/makeexam" element={<MakeExam />} />
                        <Route
                            path="/attemptedexam"
                            element={<AttemptedExam />}
                        />
                        <Route
                            path="/exams/:exam_id/createquestion"
                            element={<CreateQuestion />}
                        />
                        <Route
                            path="/takeexam/:attempt_id"
                            element={<TakeExam />}
                        />
                        <Route path="/" element={<Heropage />} />
                    </Routes>
                </BrowserRouter>
            </AuthProvider>
        </>
    )
}

export default App
