export async function login(baseUrl, username, password) {
    const url = `${baseUrl}/token/`

    const form = new FormData()
    form.append('username', username)
    form.append('password', password)
    const response = await fetch(url, {
        method: 'post',
        credentials: 'include',
        body: form,
    })
    if (!response.ok) {
        throw Error('Incorrect username or password')
    }
    const data = await response.json()
    if (data.access_token) {
        return data.access_token
    } else {
        throw Error('Failed to get token after login.')
    }
}

export async function logout(baseUrl) {
    const url = `${baseUrl}/token/`
    const fetchConfig = {
        method: 'DELETE',
        credentials: 'include',
    }
    const response = await fetch(url, fetchConfig)
    if (!response.ok) {
        throw Error('Failed to logout')
    }
}
