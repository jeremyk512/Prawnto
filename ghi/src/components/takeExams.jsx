import { useState, useEffect } from 'react'
import { useParams, useNavigate } from 'react-router-dom'
import ocean_background from '../images/ocean_background.png'
const API_HOST = import.meta.env.VITE_API_HOST

function TakeExam() {
    const [questions, setQuestions] = useState([])
    const [examId, setExamId] = useState()
    const [optionMap, setOptionMap] = useState()
    const examAttempt = useParams().attempt_id
    const [optionChoice, setOptionChoice] = useState({})
    const navigate = useNavigate()
    const [token, setToken] = useState('unassigned')

    const getData = async () => {
        const response = await fetch(
            `${API_HOST}/api/exam_attempts/exam/${examAttempt}`,
            { credentials: 'include' }
        )
        if (response.ok) {
            const data = await response.json()
            const exam_id = data[0]
            setExamId(exam_id)
        } else {
            console.error('Exam Id not retrieved')
        }
    }

    const getQuestions = async () => {
        const questionUrl = `${API_HOST}/api/questions/${examId}`
        const response = await fetch(questionUrl, { credentials: 'include' })
        if (response.ok) {
            const data = await response.json()
            setQuestions(data.questions)
        } else {
            console.error('Questions not retrieved')
        }
    }

    const fetchOptions = async (question_id) => {
        const optionsUrl = `${API_HOST}/api/options/${question_id}`
        const response = await fetch(optionsUrl, { credentials: 'include' })
        if (response.ok) {
            const options = await response.json()
            return options.options
        } else {
            console.error('Options not retrieved')
        }
    }

    const assignOptions = async () => {
        const newOptionMap = {}
        for (let question of questions) {
            const options = await fetchOptions(question.question_id)
            const optionTexts = options.map((option) => option)
            newOptionMap[question.question_id] = optionTexts
        }

        setOptionMap(newOptionMap)
    }

    const handleOnChange = async (event) => {
        let questionNum = event.target.name
        let optionNum = event.target.value
        setOptionChoice({ ...optionChoice, [questionNum]: optionNum })
    }

    const handleSubmit = async () => {
        const responseUrl = `${API_HOST}/api/responses/${examAttempt}`
        for (let option in optionChoice) {
            const responseBody = { option_id: Number(optionChoice[option]) }
            const fetchConfig = {
                method: 'post',
                credentials: 'include',
                body: JSON.stringify(responseBody),
                headers: {
                    'Content-Type': 'application/json',
                },
            }
            const response = await fetch(responseUrl, fetchConfig)
            if (!response.ok) {
                console.error('There was a problem!')
            }
        }
        const gradeUrl = `${API_HOST}/api/exam_attempts/${examAttempt}/grade`
        const gradeResponse = await fetch(gradeUrl, { credentials: 'include' })
        if (gradeResponse.ok) {
            navigate('/attemptedexam')
        } else {
            console.error(gradeResponse)
        }
    }

    const findToken = async () => {
        const tokenResponse = await fetch(`${API_HOST}/token`, {
            credentials: 'include',
        })
        if (tokenResponse.ok) {
            const tokenJson = await tokenResponse.json()
            if (tokenJson === null) {
                setToken(null)
            } else {
                setToken(tokenJson.access_token)
            }
        } else {
            console.error('There was an error fetching the token.')
        }
    }
    const checkStatus = async (token) => {
        if (!token) {
            navigate('/login')
        }
    }

    useEffect(() => {
        findToken()
        getData()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])
    useEffect(() =>{
        checkStatus(token)
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [token])
    useEffect(() => {
        if (examId){
            getQuestions()
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [examId])

    useEffect(() => {
        assignOptions()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [questions])

    return (
        <div
            className="pt-32 bg-fixed bg-cover bg-center min-h-screen"
            style={{ backgroundImage: `url(${ocean_background})` }}
        >
            <div className="p-8 max-w-3xl mx-auto space-y-6">
                {questions?.map((question, index) => (
                    <div key={index}>
                        <div className="relative bg-opacity-70 bg-white max-w-3xl mx-auto border border-slate-700 shadow-lg rounded-lg p-6 ">
                            <div className="absolute top-0 right-0 h-1/5 bg-slate-900 text-red-500 w-full rounded-t-lg text-center">
                                <h1 className="font-bold h-full">
                                    Question {index + 1}:
                                </h1>
                            </div>
                            <div className="pt-8 px-2">
                                <div>
                                    <p className="font-medium text-lg">
                                        {question.question_text}
                                    </p>
                                </div>
                            </div>
                            <div className="pt-2 pb-6">
                                {optionMap[question.question_id]?.map(
                                    (option, index) => (
                                        <div
                                            key={index}
                                            className="flex space-x-6"
                                        >
                                            <div>
                                                <input
                                                    onChange={handleOnChange}
                                                    className=""
                                                    type="radio"
                                                    name={question.question_id}
                                                    value={option.option_id}
                                                />
                                            </div>
                                            <p key={index}>
                                                {option.option_text}
                                            </p>
                                        </div>
                                    )
                                )}
                            </div>
                        </div>
                    </div>
                ))}
            </div>
            <div className="flex justify-center items-center pb-20">
                <button
                    onClick={handleSubmit}
                    className="bg-slate-900 text-red-500 font-medium text-center px-2 py-1 border border-slate-500"
                >
                    Submit Exam
                </button>
            </div>
        </div>
    )
}
export default TakeExam
