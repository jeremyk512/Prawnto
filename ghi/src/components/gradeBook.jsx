import { useParams, useNavigate } from 'react-router-dom'
import { useEffect, useState } from 'react'
import ocean_background from '../images/ocean_background.png'
const API_HOST = import.meta.env.VITE_API_HOST


const Gradebook = () => {
    const { exam_id } = useParams()
    const [attempts, setAttempts] = useState([])
    const [examDetails, setExamDetails] = useState()
    const [token, setToken] = useState('unassigned')
    const navigate = useNavigate()

    const [usernames, setUsernames] = useState()
    const getUsername = async (user_id) => {
        const response = await fetch(
            `${API_HOST}/api/get_username/${user_id}`,
            { credentials: 'include' }
        )
        if (response.ok) {
            const data = await response.json()
            return data.username
        } else {
            console.error('Failed to get username')
        }
    }

    const fetchUsernames = async () => {
        const usernamesMap = {}
        for (const attempt of attempts) {
            const username = await getUsername(attempt.user_id)
            usernamesMap[attempt.user_id] = username
        }
        setUsernames(usernamesMap)
    }

    const getExamDetails = async (exam_id) => {
        const response = await fetch(`${API_HOST}/api/exams/${exam_id}`, {
            credentials: 'include',
        })
        if (response.ok) {
            const data = await response.json()
            setExamDetails(data)
            return data.exam_name
        } else {
            console.error('Request Error')
        }
    }

    const getAllAttempts = async (exam_id) => {
        const url = `${API_HOST}/api/exam_attempts/${exam_id}`
        const attemptResponse = await fetch(url, { credentials: 'include' })
        if (attemptResponse.ok) {
            const data = await attemptResponse.json()
            setAttempts(data.exam_attempts)
        }
    }


    const findToken = async () => {
        const tokenResponse = await fetch(`${API_HOST}/token`, {
            credentials: 'include',
        })
        if (tokenResponse.ok) {
            const tokenJson = await tokenResponse.json()
            if (tokenJson === null) {
                setToken(null)
            } else {
                setToken(tokenJson.access_token)
            }
        } else {
            console.error('There was an error fetching the token.')
        }
    }


       const checkStatus = async (token) => {
           if (!token) {
               navigate('/login')
           }
       }
    useEffect(() => {
        findToken()
        getExamDetails(exam_id)
        getAllAttempts(exam_id)
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])
    useEffect(() => {
        checkStatus(token)
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [token])
    useEffect(() => {
        fetchUsernames()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [attempts])

    return (
        <>
            <div
                className="bg-fixed bg-cover bg-center min-h-screen pt-40 pb-40"
                style={{ backgroundImage: `url(${ocean_background})` }}
            >
                <h1 className="text-6xl font-bold text-white text-center pb-12">
                    Exam Attempts for {examDetails?.exam_name}
                </h1>
                <div>
                    <table className="mx-auto table-auto">
                        <thead>
                            <tr className="bg-slate-900 text-red-500">
                                <th className="border border-slate-500 p-2">
                                    Username
                                </th>
                                <th className="border border-slate-500 p-2">
                                    Score
                                </th>
                                <th className="border border-slate-500 p-2">
                                    Attempt Id
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            {attempts?.filter((attempt) => attempt.score != null).map((attempt, index) => {
                                return (
                                    <tr
                                        className="text-center items-center justify-center even:bg-white odd:dark:bg-gray-200 border-b dark:border-gray-700"
                                        key={index}
                                    >
                                        <td className="border border-black p-2">
                                            {usernames[attempt.user_id]}
                                        </td>
                                        <td className="border border-black p-2">
                                            {attempt.score}
                                        </td>
                                        <td className="border border-black p-2">
                                            {attempt.attempt_id}
                                        </td>
                                    </tr>
                                )
                            })}
                        </tbody>
                    </table>
                </div>
            </div>
        </>
    )
}

export default Gradebook
