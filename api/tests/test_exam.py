# *******************************************
# *******************************************
# *******************************************
# Unit Test made by Jim Floyd
# *******************************************
# *******************************************
# *******************************************

from authenticator import authenticator
from main import app
from queries.accounts import AccountOut
from fastapi.testclient import TestClient
from queries.exams import ExamQueries, ExamOut


client = TestClient(app)


class MockOneExam:
    def get_exam_details_by_exam_id(self, id: int):
        # Arrange
        return ExamOut(
            exam_id=id,
            exam_name="Parenting 101",
            description="Infant CPR",
            creator_id=1,
        )


def fake_get_current_account_data():
    return AccountOut(
        user_id=1,
        username="JimFlo",
        first_name="Jim",
        last_name="Floyd",
        email="JFlo@gmail.com",
    )


def test_get_one_exam():
    app.dependency_overrides[ExamQueries] = MockOneExam
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data
    id = 1
    # Act
    response = client.get(f"/api/exams/{id}")
    expected_response = {
        "exam_id": 1,
        "exam_name": "Parenting 101",
        "description": "Infant CPR",
        "creator_id": 1,
    }
    # Cleanup
    app.dependency_overrides = {}

    # Assert
    print(response.json())
    assert response.status_code == 200
    assert response.json() == expected_response
