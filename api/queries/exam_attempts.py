from pydantic import BaseModel
from queries.pool import pool
from typing import Optional


class ExamAttemptIn(BaseModel):
    user_id: int


class ExamAttemptOut(BaseModel):
    attempt_id: int
    user_id: int
    exam_id: int
    score: Optional[int]


class ExamAttemptOutList(BaseModel):
    exam_attempts: list[ExamAttemptOut]


class ExamAttemptQueries:
    def create(self, info: ExamAttemptIn, exam_id: int) -> ExamAttemptOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                        INSERT INTO Exam_Attempts (
                            user_id,
                            exam_id
                        )
                        VALUES (
                        %s,
                        %s
                        )
                        RETURNING attempt_id;
                    """,
                    [info.user_id, exam_id],
                )
                id = result.fetchone()[0]
                old_data = info.dict()
                return ExamAttemptOut(
                    attempt_id=id, exam_id=exam_id, **old_data
                )

    def get_exams_attempts_by_exam_id(
        self, exam_id: int
    ) -> ExamAttemptOutList:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    SELECT (
                        attempt_id,
                        user_id,
                        score
                    )
                    FROM Exam_Attempts WHERE exam_id = %s;
                    """,
                    [exam_id],
                )
                records = result.fetchall()
                ExamAttemptOut_list = []
                for record in records:
                    new_ExamAttemptOut = ExamAttemptOut(
                        attempt_id=record[0][0],
                        user_id=record[0][1],
                        score=record[0][2],
                        exam_id=exam_id,
                    )
                    ExamAttemptOut_list.append(new_ExamAttemptOut)
                return ExamAttemptOutList(exam_attempts=ExamAttemptOut_list)

    def get_exam_attempts_by_user_id(self, user_id: int) -> ExamAttemptOutList:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    SELECT(
                    exam_id,
                    attempt_id,
                    score
                    )
                    FROM Exam_attempts WHERE user_id = %s;
                    """,
                    [user_id],
                )
                records = result.fetchall()
                ExamAttemptOut_list = []
                for record in records:
                    new_ExamAttempt = ExamAttemptOut(
                        user_id=user_id,
                        exam_id=record[0][0],
                        attempt_id=record[0][1],
                        score=record[0][2],
                    )
                    ExamAttemptOut_list.append(new_ExamAttempt)
                return ExamAttemptOutList(exam_attempts=ExamAttemptOut_list)

    def get_exams_by_attempt_id(self, attempt_id: int):
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    SELECT
                    (exam_id) FROM exam_attempts WHERE attempt_id = %s;
                    """,
                    [attempt_id],
                )
                records = result.fetchone()
                return records

    def grade_by_attempt_id(self, attempt_id: int) -> ExamAttemptOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                # Get number of questions in the exam being attempted
                question_num = db.execute(
                    """
                    SELECT COUNT(q.*) FROM exam_attempts ea
                    INNER JOIN questions q on ea.exam_id = q.exam_id
                    WHERE ea.attempt_id = %s;
                    """,
                    [attempt_id],
                )
                question_record = question_num.fetchone()
                number_of_questions = question_record[0]

                # Get number of correct responses in the exam attempt
                correct_responses = db.execute(
                    """
                    SELECT SUM(
                        case when Options.correct = True then 1 else 0 end
                        )
                    FROM Responses
                    INNER JOIN options on
                    responses.option_id = options.option_id
                    WHERE responses.attempt_id = %s;
                    """,
                    [attempt_id],
                )
                correct_record = correct_responses.fetchone()
                number_correct = correct_record[0]

                if number_correct is None:
                    number_correct = 0
                # calculate final grade,
                # rounded to nearest number, between 0-100
                final_grade = round(
                    (number_correct / number_of_questions) * 100
                )

                db.execute(
                    """
                    UPDATE exam_attempts
                    SET Score = %s
                    WHERE attempt_id = %s;
                    """,
                    [final_grade, attempt_id],
                )

                # get result to output in ExamAttemptOut
                result = db.execute(
                    """
                    SELECT (
                        user_id,
                        exam_id,
                        score
                    )
                    FROM Exam_Attempts WHERE attempt_id = %s;
                    """,
                    [attempt_id],
                )
                new_record = result.fetchone()[0]
                return ExamAttemptOut(
                    attempt_id=attempt_id,
                    user_id=new_record[0],
                    exam_id=new_record[1],
                    score=new_record[2],
                )
