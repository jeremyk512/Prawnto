from pydantic import BaseModel
from queries.pool import pool


class ResponseIn(BaseModel):
    option_id: int


class ResponseOut(BaseModel):
    response_id: int
    attempt_id: int
    option_id: int


class ResponseQueries:
    def create(self, info: ResponseIn, attempt_id: int) -> ResponseOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                        INSERT INTO Responses (
                        option_id,
                        attempt_id
                        )
                        VALUES (
                        %s,
                        %s
                        )
                        RETURNING response_id;
                    """,
                    [
                        info.option_id,
                        attempt_id,
                    ],
                )
                id = result.fetchone()[0]
                old_data = info.dict()
                return ResponseOut(
                    response_id=id, attempt_id=attempt_id, **old_data
                )
