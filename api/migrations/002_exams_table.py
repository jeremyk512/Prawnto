steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE Exams (
            exam_id SERIAL PRIMARY KEY,
            exam_name VARCHAR(200),
            description VARCHAR(500),
            creator_id INT REFERENCES Accounts_with_hashed_passwords(user_id)
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE Exams;
        """,
    ],
]
