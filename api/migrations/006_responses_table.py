steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE Responses (
            response_id SERIAL PRIMARY KEY,
            attempt_id INT REFERENCES Exam_Attempts(attempt_id),
            option_id INT REFERENCES Options(option_id)
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE Responses;
        """,
    ]
]
