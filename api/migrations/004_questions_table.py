steps = [
    [
        """
        CREATE TABLE Questions (
            question_id SERIAL PRIMARY KEY,
            exam_id INT REFERENCES Exams(exam_id),
            question_text VARCHAR(500) NOT NULL
        );
        """,
        """
        DROP TABLE Questions;
        """,
    ]
]
