steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE Accounts_with_hashed_passwords (
            user_id SERIAL PRIMARY KEY,
            hashed_password VARCHAR(64),
            first_name VARCHAR(50),
            last_name VARCHAR(50),
            username VARCHAR(16),
            email VARCHAR(50)
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE Accounts_with_hashed_passwords;
        """,
    ],
]
