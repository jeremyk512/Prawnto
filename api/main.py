from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
import os
from authenticator import authenticator
from routers import (
    accounts,
    exams,
    questions,
    exam_attempts,
    options,
    responses,
)

app = FastAPI()
app.include_router(authenticator.router)
app.include_router(accounts.router)
app.include_router(exams.router)
app.include_router(questions.router)
app.include_router(exam_attempts.router)
app.include_router(options.router)
app.include_router(responses.router)


app.add_middleware(
    CORSMiddleware,
    allow_origins=[
        os.environ.get("CORS_HOST"),
        (
            "https://prawnto-mantis-shrimp1-"
            + "7ce913ce20e288a2cb4533210c81ea6e720e3afc.gitlab.io"
        ),
    ],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
