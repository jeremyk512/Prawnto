Jan 16th:
    Worked on setting up Docker and PostgreSQL Database

Jan 17th:
    Team worked on and completed Login and Loguut endpoints. Team continues to work on Create Account Endpoint

Jan 18th:
    Worked with team to complete Create Account endpoint. Team began and completed work on building out tables for database.

Jan 19th:
    Worked with team building out endpoints. Worked on get questions by exam_id, Create empty exam question, updating questions path enpoints

Jan 21st:
    Create view all exam attempts by user_id enpoint, worked on require authentication for all enpoints, installing and accessing pgAdmin.

Jan 22nd:
    Implemented authentication required for all endpoints except Login, and Create Account.

Jan 23rd:
    Finished all known endpoints at this time. Worked with Jim on login page learning Tailwind CSS.

Jan 24th:
    Pair programmed with Jim working on log in authenication for the front-end, hitting significant blockers.

jan 25th:
    Continue to troubleshoot login functionality, and get it resolved. Pair programmed with Jim working on adding make exam page.

Jan 26th:
    Worked on locking down api endpoints ensuring only those authenticated can access them.

Jan 29th:
    Added backend to get User Data by user_id. Added dropdown menu to front-end. Created Create Exam Attempt on explore exams.

Jan 30th:
    Added endpoint to get question id by exam id. Worked on getting questions, and question options and mapping them to the page.

Jan 31st:
    Pair programmed with Jim to get hero page template in place. Mob programmed with Jim, Jeremy and Sam got radio buttons in place on take exam, and placed submit test button which automatically grades exam, and navigates you to the assessments page.

Feb 1st:
    Pair programmed with Sam. I drove building out the gradebook webpage

Feb 5th:
    Started working on CI/CD. We each took turns building out part of the deployment. Hit several blockers and will continue tomorrow.

Feb 6th:
    Continued working on deployment. Hit significant road blocks with cors host errors, and images not appearing. Worked with SEIRS and the team was able to resolve the problems.

Feb 7th:
    Worked on code clean up, and fixing minor issues with front-end.

Feb 8th:
    Fianl clean up
Feb 9th:
