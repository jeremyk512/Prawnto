<center>

# Wireframe
#### This shows the navigational links between each of our webpages

![Alt text](../ghi/src/images/entireWireFrame1.png)

# Hero Page
#### This our home page where users can get started by signing up or logging in if they already have an account.

 ![Hero page](../ghi/src/images/heroPage.png)

![Alt text](../ghi/src/images/heroPage_1.png)

# Login Page
#### This Login page directs users to enter their username and password

![Login page](../ghi/src/images/loginPage.png)

![Alt text](../ghi/src/images/loginPage_1.png)

# Signup Page
#### This page allows users to sign up for a new website by entering the prompted information

![Sign up](../ghi/src/images/signUp.png)

![Alt text](../ghi/src/images/signUp_1.png)

# Welcome Page
#### Our welcome page allows users to select and take an exam

![Welcome page](../ghi/src/images/welcomePage.png)

![Alt text](../ghi/src/images/welcomePage_1.png)

# Exam Page
#### The Exam page displays questions and options for the exam and users can select submit exam where they're finished

![Exam page](../ghi/src/images/exam.png)

![Alt text](../ghi/src/images/exam_1.png)

# Attempted Exams Page
#### After submitting an exam, the exam results and exam title will populate on this page as a way for the user to keep track of what exams they have taken

![Attempted Exam](../ghi/src/images/attemptedExams.png)

![Alt text](../ghi/src/images/attemptedExams_1.png)

# Manage Exam Page
#### The manage exam page allows users to see what exams they have created and can select a gradebook button for each created exam

![Manage Exam](../ghi/src/images/manageExams.png)

![Alt text](../ghi/src/images/manageExams_1.png)

# Gradebook
#### The gradebook page shows the username, score and attempt number for the particular exam created

![Gradebook](../ghi/src/images/gradebook.png)

![Alt text](../ghi/src/images/gradebook_1.png)

# Make Exam
#### This page, users can enter the exam name and description for the exam they are creating

![Make Exam](../ghi/src/images/makeExam.png)

![Alt text](../ghi/src/images/makeExam_1.png)

# Make Exam Questions
#### Users will enter questions and options for the exam. By toggling the correct option will set answer to true

![Make Exam Questions](../ghi/src/images/makeExamQuestions.png)

![Alt text](../ghi/src/images/makeExamQuestions_1.png)
